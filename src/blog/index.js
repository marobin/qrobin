import FirstBlog from 'src/blog/FirstBlog.md'
import SecondBlog from 'src/blog/SecondBlog.md'

function apercu (blog) {
  return (blog.substr(0, 100))
}

export default [apercu(FirstBlog), apercu(SecondBlog)]
