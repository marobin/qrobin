import * as firebase from 'firebase/app'
import 'firebase/auth'

var store = {
  debug: true,
  state: {
    user: undefined
  },
  async init () {
    if (firebase.apps.length === 0) {
      var config = {
        databaseURL: 'https://qrobin-5af96.firebaseio.com',
        apiKey: 'AIzaSyCy_8fKc0JEEUX6Lz5p8E1az_ZfG16KihY',
        authDomain: 'qrobin-5af96.firebaseapp.com',
        projectId: 'qrobin-5af96',
        storageBucket: 'qrobin-5af96.appspot.com',
        messagingSenderId: '565608038960',
        appId: '1:565608038960:web:6998d02b06bad8a17423f5',
        measurementId: 'G-P74P5TVHF5'
      }
      firebase.initializeApp(config)
    }
    var user = firebase.auth().currentUser
    if (user) {
      this.state.user = user
    }
  },
  async login (username, password) {
    firebase.auth().signInWithEmailAndPassword(username, password).catch((error) => {
      console.log(error)
    }).then(() => {
      var user = firebase.auth().currentUser
      if (user) {
        this.state.user = user
      }
      return user
    })
  },
  async logout () {
    firebase.auth().signOut()
    this.state.authenticated = false
    return true
  }
}

export default store
